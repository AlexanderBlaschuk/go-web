#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDesktopServices>
#include <QUrl>
#include <QIcon>
#include <QMessageBox>
#include <QTimer>
#include <QLabel>
#include <Phonon/VideoPlayer>
#include <QProcess>
#include <QFile>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    currentFile = "";
    this->connect(this,SIGNAL(gotMediaList()),SLOT(on_gotMediaList()));
    QTimer::singleShot(1200000,this,SLOT(close()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::parseUrl(QString& url)
{
    url = url.replace("\\","");
}

void MainWindow::on_tableWidget_cellPressed(int row, int column)
{
    currentFile = mediaFiles[row].fileAUrl;

    if (column == 0)
    {
        if (currentFile.isEmpty())
        {
            return;
        }
        QDesktopServices::openUrl(QUrl(currentFile));
    }
}

void MainWindow::on_loginButton_clicked()
{
    ui->tableWidget->clear();
    mediaFiles.clear();

    if (ui->loginEdit->text().isEmpty() || ui->passwordEdit->text().isEmpty())
    {
        QMessageBox mb(QMessageBox::Warning,tr("Error"),tr("Please, fill the fields for login"),QMessageBox::Ok,this);
        mb.exec();
        return;
    }

    getFilesFromServer();
}

void MainWindow::on_gotMediaList()
{
    ui->tableWidget->clear();

    while(ui->tableWidget->rowCount() != 0)
    {
        ui->tableWidget->removeRow(0);
    }

    int rowCounter = 0;
    foreach(FileInfo file,mediaFiles)
    {
        ui->tableWidget->insertRow(rowCounter);
        QPixmap p(":/images/playIcon");
        QIcon icon;
        icon.addPixmap(p);
        QTableWidgetItem *newItem = new QTableWidgetItem(icon,"");
        ui->tableWidget->setItem(rowCounter, 0, newItem);
        QString fileDescription = "Title : " + file.fileTitle + "\n" +
                                  "Artist: " + file.fileArtist + "\n" +
                                  "Genre :  " + file.fileGenre;
        newItem = new QTableWidgetItem(fileDescription);
        ui->tableWidget->setItem(rowCounter, 1, newItem);
        ++rowCounter;
    }
}

void MainWindow::getFilesFromServer()
{
    QNetworkAccessManager* pManager = new QNetworkAccessManager();
    this->connect(qobject_cast<QObject *>(pManager), SIGNAL(finished(QNetworkReply*)), SLOT(postReplyFinish(QNetworkReply*)));
    pManager->post( QNetworkRequest(QUrl("http://vacancy.dev.telehouse-ua.net/auth/login")), QString(QString("login=") + ui->loginEdit->text() + QString("&password=") + ui->passwordEdit->text()).toUtf8());
}

void MainWindow::postReplyFinish(QNetworkReply* netReply)
{
    QString stringReply = QString::fromUtf8(netReply->readAll());

    if (netReply->error() != 0)
    {
        QMessageBox mb(QMessageBox::Warning,"Network error",stringReply,QMessageBox::Ok);
        mb.exec();
    } else
    {
        QNetworkAccessManager* pManager = new QNetworkAccessManager();
        this->connect(qobject_cast<QObject *>(pManager), SIGNAL(finished(QNetworkReply*)), SLOT(getReplyFinish(QNetworkReply*)));
        QNetworkRequest getRequest(QUrl("http://vacancy.dev.telehouse-ua.net/media/list"));
        int startIndex = stringReply.indexOf(',') + 10;
        int amount = stringReply.indexOf('}') - startIndex - 1;
        QString tokenString = stringReply.mid(startIndex,amount);

        QByteArray rawHeader;
        rawHeader.append(tokenString);
        getRequest.setRawHeader("X-Auth-Token",rawHeader);
        pManager->get(getRequest);
    }

    netReply->close();
}

void MainWindow::getReplyFinish(QNetworkReply* netReply)
{
    QString replyString = QString::fromUtf8(netReply->readAll());

    if (netReply->error() != 0)
    {
        QMessageBox mb(QMessageBox::Warning,"Network error",replyString,QMessageBox::Ok);
        mb.exec();
    }else
    {
        fillMediaList(replyString);
    }
    netReply->close();
}

void MainWindow::fillMediaList(QString replyString)
{
    mediaFiles.clear();

    int startIndex = replyString.indexOf("{",1);
    int endIndex = replyString.indexOf("}",1);
    int charsAmount = endIndex - startIndex + 1;
    QString subString = replyString.mid(startIndex,charsAmount);
    QString bufferString = replyString.right(replyString.length() - startIndex - 2);

    while (endIndex != bufferString.length() - 3)
    {
        int indexOfDoubleDot = 0;
        int indexOfComa = 0;
        int subStringLength = subString.length();
        FileInfo currentFileInfo;
        currentFileInfo.fileAUrl = getNextStringValue(indexOfDoubleDot,indexOfComa,subString).trimmed();
        currentFileInfo.fileIUrl = getNextStringValue(indexOfDoubleDot,indexOfComa,subString).trimmed();
        currentFileInfo.fileGenre = getNextStringValue(indexOfDoubleDot,indexOfComa,subString).trimmed();
        currentFileInfo.fileArtist = getNextStringValue(indexOfDoubleDot,indexOfComa,subString).trimmed();
        currentFileInfo.fileTitle = getNextStringValue(indexOfDoubleDot,indexOfComa,subString).trimmed();
        parseUrl(currentFileInfo.fileAUrl);
        parseUrl(currentFileInfo.fileIUrl);
        mediaFiles.append(currentFileInfo);

        bufferString = bufferString.mid(subStringLength + 1);
        startIndex = bufferString.indexOf("{");
        endIndex = bufferString.indexOf("}");
        charsAmount = endIndex - startIndex + 1;
        subString = bufferString.mid(startIndex,charsAmount);
    }

    emit(gotMediaList());

}

QString MainWindow::getNextStringValue(int &startIndex, int &endIndex, QString &subString)
{
    startIndex = subString.indexOf(':');
    endIndex = subString.indexOf(',');
    QString resultString = subString.mid(startIndex + 2,endIndex - startIndex - 3);
    int charsAmount = subString.length() - endIndex - 2;
    if (charsAmount > 0)
    {
        subString = subString.right(charsAmount);
    } else
    {
        subString = "";
    }
    int indexOfEnd = resultString.indexOf("}");
    while (indexOfEnd > 0)
    {
        resultString = resultString.left(resultString.length() - 2);
        indexOfEnd = resultString.indexOf("}");
    }
    resultString = resultString.replace("\"","");

    return resultString;
}
