#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QList>
#include <QString>
#include <QtNetwork/QNetworkReply>

#include "fileinfo.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void getFilesFromServer();
    void fillMediaList(QString replyString);
    QString getNextStringValue(int &startIndex, int &endIndex, QString &subString);

private:
    Ui::MainWindow *ui;
    QString currentFile;
    QList<FileInfo> mediaFiles;
    void parseUrl(QString& url);

signals:
    void gotMediaList();

private slots:

    void on_tableWidget_cellPressed(int row, int column);
    void on_loginButton_clicked();
    void postReplyFinish(QNetworkReply* netReply);
    void getReplyFinish(QNetworkReply* netReply);
    void on_gotMediaList();
};

#endif // MAINWINDOW_H
