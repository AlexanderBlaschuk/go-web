#ifndef FILEINFO_H
#define FILEINFO_H

#include <QString>
#include <QFile>
#include <QFileInfo>

class FileInfo
{
public:
    FileInfo();
    QString fileAUrl;
    QString fileIUrl;
    QString fileGenre;
    QString fileArtist;
    QString fileTitle;
};

#endif // FILEINFO_H
