#-------------------------------------------------
#
# Project created by QtCreator 2013-07-23T09:01:35
#
#-------------------------------------------------

QT       += core gui \
            network \
            phonon \
            webkit

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MediaViewer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    fileinfo.cpp

HEADERS  += mainwindow.h \
    fileinfo.h

FORMS    += mainwindow.ui

RESOURCES += \
    mediaviewer.qrc
